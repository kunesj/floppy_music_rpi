#!/usr/bin/python2
# coding: utf-8

import sys
import time
import traceback
import argparse

import pygame
import pygame.midi

import socket 
import pickle

def get_port_list():
    port_list = []
    i = 0
    while True:
        info = pygame.midi.get_device_info(i)
        if info is None:
            break
        elif info[3] == 1:
            port_list.append([i, info])
        i += 1

    return port_list

def select_port():
    print "printing info about device ids"
    ports = get_port_list()
    for p in ports:
        print 'id:', p[0], 'info:', p[1]

    selected_id = int(raw_input('Select id: '))

    return selected_id

def test_port(port):
    ports = get_port_list()
    correct = False
    for p in ports:
        if p[0] == port:
            correct = True
            break

    return correct

def main():
    # input parser
    parser = argparse.ArgumentParser(
        description='Floppy midi player - midi over network (server)'
    )
    parser.add_argument(
        '-p', '--port',
        type=int,
        default=None,
        help='Select midi port to use. Set "Midi Through" on this and midikeyboard.py for proper connection')
    parser.add_argument(
        '-l', '--listports',
        action='store_true',
        help='Lists availible midi ports')
    parser.add_argument(
        '-v', '--verbose',
        action='store_true',
        help='verbose')
    parser.add_argument(
        '--nport',
        type=int,
        default=62284,
        help='Select network port to use')
    parser.add_argument(
        '--ip',
        type=str,
        default="127.0.0.1",
        help='Server IP adress to use')

    args = parser.parse_args()

    #init pygame.midi
    pygame.midi.init()

    # list ports
    if args.listports:
        ports = get_port_list()
        for p in ports:
            print 'id:', p[0], 'info:', p[1]
        sys.exit()

    # select port
    mport = args.port
    while not test_port(mport):
        if mport is not None:
            print "Bad selected port"
        mport = select_port()
    
    # open port
    out_port = pygame.midi.Output(mport)
        
    # socket for network connections 
    serversocket =socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    print "Listening on: "+str(args.nport)
    serversocket.bind(("", args.nport))  
    
    # get notes to be played
    print "Start processing loop"
    try:
        while True:
            data, addr = serversocket.recvfrom(10240) # TODO - unpacked events and iterate over them
            events = pickle.loads(data) 
            
            if args.verbose:
                print events
                #print str(data).strip(), str(addr).strip()
            
            for e in events:
                event = e[0]
                if event[0] >= 128 and event[0] <= 143:
                    channel = event[0]-128
                    out_port.note_off(event[1], event[2], channel)
                elif event[0] >= 144 and event[0] <= 159:
                    channel = event[0]-144
                    out_port.note_on(event[1], event[2], channel)            
            
    except Exception, e: 
        print traceback.format_exc()
        serversocket.close()
        out_port.close()

if __name__ == "__main__":
    main()

