#!/usr/bin/python2
import time
import argparse
import sys
import traceback

import floppy_sound_driver as driver
import pygame 
import pygame.midi

PIN_DIR = [8,7,16,1] # direction pins for floppy drives
PIN_STEP = [9,15,0,2] # step pins for floppy drives

# periods for each midi note no. 
# periods are in mikroseconds
basicPeriods = [122312, 115444, 108968, 102852, 97080, 91636, 86488, 81636, 77052, 72728, 68644, 64792] # C-1 - B-1

microPeriods = []
for j in range(0,9):
    for i in range(0,12):
        microPeriods.append(int(basicPeriods[i]/(2**j)))

# save keystate (on/off)
keylist = [] 
for i in range(150):
    keylist.append(False)

def get_port_list():
    port_list = []
    i = 0
    while True:
        info = pygame.midi.get_device_info(i)
        if info is None:
            break
        elif info[2] == 1:
            port_list.append([i, info])
        i += 1
            
    return port_list

def select_port():
    print "printing info about device ids"
    ports = get_port_list()
    for p in ports:
        print 'id:', p[0], 'info:', p[1]

    selected_id = int(raw_input('Select id: '))
    
    return selected_id

def test_port(port):
    ports = get_port_list()
    correct = False
    for p in ports:
        if p[0] == port:
            correct = True
            break
    
    return correct

def main():
    # input parser
    parser = argparse.ArgumentParser(
        description='Floppy midi player'
    )
    parser.add_argument(
        '-p', '--port',
        type=int,
        default=None,
        help='Select port to use')
    parser.add_argument(
        '-l', '--listports',
        action='store_true',
        help='Lists availible midi ports')
    parser.add_argument(
        '-v', '--verbose',
        action='store_true',
        help='verbose')

    args = parser.parse_args()

    pygame.midi.init()
    
    # list ports
    if args.listports:
        ports = get_port_list()
        for p in ports:
            print 'id:', p[0], 'info:', p[1]
        sys.exit()
    
    # select port
    port = args.port
    while not test_port(port):
        if port is not None:
            print "Bad selected port"
        port = select_port()
        
    # start floppy driver
    driver.init(PIN_DIR,PIN_STEP)
    driver.resetAll()
    driver.run()
    
    # current played note
    currentNote = []
    for i in range(len(PIN_DIR)):
            currentNote.append(None)
        
    # start loop
    print "openning port", port
    keyboard = pygame.midi.Input(port)
    try:
        while True:
            if keyboard.poll():
                events = keyboard.read(100)
                for e in events:
                    if args.verbose:
                        print e
                    if e[0][0] >= 144 and e[0][0] <= 159: # note on (note_no = e[0][1])
                        if e[0][2]!=0:
                            keylist[e[0][1]] = True
                        else: 
                            # if velocity = 0, turn note off
                            keylist[e[0][1]] = False
                    elif e[0][0] >= 128 and e[0][0] <= 143: # note off (note_no = e[0][1])
                        keylist[e[0][1]] = False
                
                newNote = []
                for i in range(len(keylist)):
                    if keylist[i]:
                        newNote.append(i)
                
                # turn off old keys
                for i in range(len(currentNote)):
                    if (currentNote[i] is not None) and (currentNote[i] not in newNote):
                        # old key was turned off
                        currentNote[i] = None
                        driver.setPeriod(i, 0)
                
                # turn on new keys
                for i in range(len(newNote)):
                    # check if thre are any drives availible
                    if None not in currentNote:
                        break 
                    
                    if newNote[i] not in currentNote:
                        # new key was pressed
                        for n in range(len(currentNote)):
                            if currentNote[n] is None:
                                break
                        
                        currentNote[n] = newNote[i]
                        driver.setPeriod(n, microPeriods[newNote[i]])
                
    except Exception, e: 
        print traceback.format_exc()
        driver.stop()
        time.sleep(0.2)
        keyboard.close()
    
if __name__ == "__main__":
    main()
