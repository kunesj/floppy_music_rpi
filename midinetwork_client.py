#!/usr/bin/python2
import time
import argparse
import sys
import traceback

import pygame 
import pygame.midi

import socket
import pickle

def get_port_list():
    port_list = []
    i = 0
    while True:
        info = pygame.midi.get_device_info(i)
        if info is None:
            break
        elif info[2] == 1:
            port_list.append([i, info])
        i += 1
            
    return port_list

def select_port():
    print "printing info about device ids"
    ports = get_port_list()
    for p in ports:
        print 'id:', p[0], 'info:', p[1]

    selected_id = int(raw_input('Select id: '))
    
    return selected_id

def test_port(port):
    ports = get_port_list()
    correct = False
    for p in ports:
        if p[0] == port:
            correct = True
            break
    
    return correct

def send_msg(host, port, msg):
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
        sock.sendto(msg, (host, port))
    except Exception, e:
        print "Failed to send msg!"
        print traceback.format_exc()

def main():
    # input parser
    parser = argparse.ArgumentParser(
        description='Floppy midi player'
    )
    parser.add_argument(
        '-p', '--port',
        type=int,
        default=None,
        help='Select midi port to use')
    parser.add_argument(
        '-l', '--listports',
        action='store_true',
        help='Lists availible midi ports')
    parser.add_argument(
        '-v', '--verbose',
        action='store_true',
        help='verbose')
    parser.add_argument(
        '--nport',
        type=int,
        default=62284,
        help='Select network port to use')
    parser.add_argument(
        '--ip',
        type=str,
        default="127.0.0.1",
        help='Server IP adress to use')

    args = parser.parse_args()

    pygame.midi.init()
    
    # list ports
    if args.listports:
        ports = get_port_list()
        for p in ports:
            print 'id:', p[0], 'info:', p[1]
        sys.exit()
    
    # select port
    mport = args.port
    while not test_port(mport):
        if mport is not None:
            print "Bad selected port"
        mport = select_port()
        
    # open midi port
    print "openning port", mport
    keyboard = pygame.midi.Input(mport)
    
    #print info
    print "Server connection info: "+args.ip+":"+str(args.nport)
    
    # turn off all keys on server
    #print "Turning off all keys on server"
    #for i in range(0, 9*12):
        #send_msg(args.ip, args.nport, "[128, "+str(i)+", 0, 0]")  
    
    # start loop
    print "Starting loop"
    try:
        while True:
            if keyboard.poll():
                events = keyboard.read(100)
                print len(events) # TODO send packed events to server
                if args.verbose:
                    print events
                
                pickled = pickle.dumps(events)
                send_msg(args.ip, args.nport, pickled)           

    except Exception, e: 
        print traceback.format_exc()
        keyboard.close()
    
if __name__ == "__main__":
    main()
