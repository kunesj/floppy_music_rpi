#!/usr/bin/python2
import floppy_sound_driver as driver
import time
import sys

# uses wiringPi pinout
# indexes of the pins for same drive shuld the same
direction_pins = [8,7]
step_pins = [9,15]

driver.init(direction_pins,step_pins)

print "Reseting drives to starting position"
driver.resetAll()

print "Starting driver loop..."
driver.run()

print "Set period of drive 1 to 2273"
driver.setPeriod(1, 2273)
time.sleep(4)

print "Set period of drive 0 to 1500"
driver.setPeriod(0, 1500)
time.sleep(4)

print "Turn off drive 1"
driver.setPeriod(1, 0)
time.sleep(4)

print "Stop driver loop"
driver.stop()
time.sleep(0.2)
sys.exit()
