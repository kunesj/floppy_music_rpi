#!/usr/bin/python2

from distutils.core import setup, Extension

module1 = Extension('floppy_sound_driver',
                    define_macros = [('MAJOR_VERSION', '1'),
                                     ('MINOR_VERSION', '0')],
                    include_dirs = ['/usr/local/include'],
                    libraries = ['python2.7','wiringPi','rt'],
                    library_dirs = ['/usr/local/lib'],
                    sources = ['floppy_sound_driver.c', 'floppy_sound_driver_py.c'])

setup (name = 'floppy_sound_driver',
       version = '1.0',
       description = 'Floppy sound driver',
       author = 'Jiri Kunes',
       author_email = 'jirka642@gmail.com',
       url = 'https://github.com/kunesj/floppy_music_rpi',
       long_description = '''
Floppy sound driver.
''',
       ext_modules = [module1])
