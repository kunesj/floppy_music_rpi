#include "floppy_sound_driver_py.h"
#include "floppy_sound_driver.h"

/*
*  Python module main
*/ 
    
int main(int argc, char *argv[]){
    /* Pass argv[0] to the Python interpreter */
    Py_SetProgramName(argv[0]);

    /* Initialize the Python interpreter.  Required. */
    Py_Initialize();

    /* Add a static module */
    initfloppy_sound_driver();
    
    Py_Exit(0);
    return 0;
    }
    
/*
*  Static module
*/

static PyObject * init_py(PyObject *self, PyObject *args){   
    PyObject *direction_pins_obj;
    PyObject *step_pins_obj;
    PyArg_ParseTuple(args, "OO", &direction_pins_obj, &step_pins_obj);
    
    int direction_pins[13];
    int step_pins[13];
    
    PyObject *i = PyObject_GetIter(direction_pins_obj);
    PyObject *n = PyObject_GetIter(step_pins_obj);

    int len = 0;
    while (TRUE) {
        PyObject *next = PyIter_Next(i);
        if (!next) {
            // nothing left in the iterator
            break;
        }

        direction_pins[len] = (int)PyInt_AsLong(next);
        len++;
    }
    
    len = 0;
    while (TRUE) {
        PyObject *next = PyIter_Next(n);
        if (!next) {
            // nothing left in the iterator
            break;
        }

        step_pins[len] = (int)PyInt_AsLong(next);
        len++;
    }
    
    init(direction_pins, step_pins, len);
    
    return Py_BuildValue(""); // return None
    }

static PyObject * resetAll_py(void){
    resetAll();
    return Py_BuildValue(""); // return None
    }

static PyObject * setPeriod_py(PyObject *self, PyObject *args){ 
    int drive_num, periode;
    PyArg_ParseTuple(args, "ii", &drive_num, &periode);
    setPeriod(drive_num,periode);
    
    return Py_BuildValue(""); // return None
    }

static PyObject * run_py(void){
    run_t();
    return Py_BuildValue(""); // return None
    }

static PyObject * stop_py(void){
    stop();
    return Py_BuildValue(""); // return None
    }

/*
*  Python module init
*/ 

static PyMethodDef floppy_sound_driverMethods[] = {
    {"init",  (PyCFunction)init_py, METH_VARARGS, "define drive pins and number of drives"},
    {"setPeriod",  (PyCFunction)setPeriod_py, METH_VARARGS, "set period of specifited drive"},
    {"resetAll",  (PyCFunction)resetAll_py, METH_VARARGS, "reset position of all drives"},
    {"run",  (PyCFunction)run_py, METH_VARARGS, "start driver loop"},
    {"stop",  (PyCFunction)stop_py, METH_VARARGS, "stop driver loop"},
    {NULL, NULL, 0, NULL}        /* Sentinel */

};

PyMODINIT_FUNC initfloppy_sound_driver(void){
    (void) Py_InitModule("floppy_sound_driver", floppy_sound_driverMethods);
    }


