#include "floppy_sound_driver.h"

int i,n = 0;
struct timespec tstart={0,0}, tend={0,0};
double tdelta;
double sleep_time;
long sleep_time_nano;

pthread_t pth;	// this is our thread identifier

// tick resolution in mikroseconds
int RESOLUTION = 40; // 40;

// RaspberryPi has 26 GPIO pins => max num of drives is 13
int num_of_drives = 0; 
int direction_pins[13]; // direction pins for floppy drives
int step_pins[13];  // step pins for floppy drives

int MAX_POSITION = 158;
int currentPosition[13];
int currentState[13];
int currentPeriod[13];
int newPeriod[13];
bool changesToCommit = FALSE;
bool stop_bol = TRUE;
bool currentPeriodState[13];
int currentTick[13];

extern void init(int direction_pins_new[13], int step_pins_new[13], int num_of_drives_new){
    clear_values();
    
    num_of_drives = num_of_drives_new;
    memcpy(direction_pins, direction_pins_new, 13);
    memcpy(step_pins, step_pins_new, 13);
    
    wiringPiSetup();
    for (i = 0; i < num_of_drives; i+=1){
        pinMode(direction_pins[i], OUTPUT);
        pinMode(step_pins[i], OUTPUT);
        }
    }
    
extern void clear_values(void){
    num_of_drives = 0;
    changesToCommit = FALSE;
    stop_bol = TRUE;
    
    memset(direction_pins, 0, 13);
    memset(step_pins, 0, 13);
    memset(currentPosition, 0, 13);
    memset(currentState, 0, 13);
    memset(currentPeriod, 0, 13);
    memset(newPeriod, 0, 13);
    memset(currentPeriodState, FALSE, 13);
    memset(currentTick, 0, 13);    
    }

extern void resetDrive(int drive_num){
    digitalWrite(direction_pins[drive_num], HIGH); // Direction (True = backwards)
    for (n = 0; n < 80; n+=1){
        digitalWrite(step_pins[drive_num], HIGH); // Step
        nanosleep((struct timespec[]){{0, 500000L}}, NULL);
        digitalWrite(step_pins[drive_num], LOW);
 
        nanosleep((struct timespec[]){{0, 5000000L}}, NULL);
        }
    currentPosition[drive_num] = 0;
    currentState[drive_num] = FALSE; // ready to go forward
    digitalWrite(direction_pins[drive_num], LOW);
    }

extern void resetAll(void){
    for (i = 0; i < num_of_drives; i+=1){
        resetDrive(i);
        }
    sleep(1);
    }

extern void toggleDrive(int drive_num){
    // change direction at end
    if (currentPosition[drive_num] >= MAX_POSITION){
        currentState[drive_num] = TRUE; 
        digitalWrite(direction_pins[drive_num], HIGH);
        }
    else if (currentPosition[drive_num] <= 0){
        currentState[drive_num] = FALSE;
        digitalWrite(direction_pins[drive_num], LOW); 
        }
    
    // Step
    digitalWrite(step_pins[drive_num], HIGH && currentPeriodState[drive_num]);
    currentPeriodState[drive_num] = !currentPeriodState[drive_num];

    // Update position
    if (currentState[drive_num] == TRUE){
        currentPosition[drive_num] -= 1;
        }
    else{
        currentPosition[drive_num] += 1;
        }
    }

extern void tick(void){
    for (i = 0; i < num_of_drives; i+=1){
        if (currentPeriod[i] > 0){
            currentTick[i] += 1;
            if (currentTick[i] >= currentPeriod[i]){
                toggleDrive(i);
                currentTick[i] = 0;
                }
            }
        }
    }

extern void setPeriod(int drive_num, int period){ 
    newPeriod[drive_num] = (int)(period / (double)((double)RESOLUTION * 2.0));
    changesToCommit = TRUE;
    }

extern void commitChanges(void){
    memcpy(currentPeriod, newPeriod, 13);
    changesToCommit = FALSE;
    }

extern void* run(void *arg){
    stop_bol = FALSE;
    while (stop_bol == FALSE){
        clock_gettime(CLOCK_MONOTONIC, &tstart);

        if (changesToCommit){
            commitChanges();
            }
        
        tick();
        
        // get how long took to process tick();
        clock_gettime(CLOCK_MONOTONIC, &tend);
        tdelta = ((double)tend.tv_sec + 1.0e-9*tend.tv_nsec) - ((double)tstart.tv_sec + 1.0e-9*tstart.tv_nsec);
        
        // get correct sleep time
        sleep_time = ((double)RESOLUTION / 1000000.0) - tdelta; // in seconds
        sleep_time_nano = (long)(sleep_time * 1000000000.0); // in nanoseconds
        
        if (sleep_time <= 0){
            continue;
            }
        
        //printf("%d\n", (int)sleep_time_nano); // debug
        nanosleep((struct timespec[]){{0, sleep_time_nano}}, NULL); // sleep in nanoseconds
        }
    return NULL;
    }
    
extern void run_t(void){
    pthread_create(&pth,NULL,&run,NULL);
    }

extern void stop(void){
    stop_bol = TRUE;
    //pthread_cancel(pth);
    }
    
