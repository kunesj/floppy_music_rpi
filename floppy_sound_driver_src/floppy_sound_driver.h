#ifndef FLOPPY_SOUND_DRIVER_H_
#define FLOPPY_SOUND_DRIVER_H_

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

#include <pthread.h>
#include <wiringPi.h>

// define bool type
typedef int bool;
#define TRUE 1
#define FALSE 0

extern void init(int direction_pins[], int step_pins[], int num_of_drives); 
extern void clear_values(void);
extern void resetDrive(int drive_num);
extern void resetAll(void);
extern void toggleDrive(int drive_num);
extern void tick(void);
extern void setPeriod(int drive_num, int period);
extern void commitChanges(void);
extern void* run(void *arg);
extern void run_t(void);
extern void stop(void);

#endif
