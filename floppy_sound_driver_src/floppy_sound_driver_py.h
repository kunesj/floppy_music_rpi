#ifndef FLOPPY_SOUND_DRIVER_PY_H_
#define FLOPPY_SOUND_DRIVER_PY_H_

#include <Python.h> // includes <stdio.h>, <string.h>, <errno.h>, <stdlib.h>

// define bool type
typedef int bool;
#define TRUE 1
#define FALSE 0

PyMODINIT_FUNC initfloppy_sound_driver(void);

static PyObject * init_py(PyObject *self, PyObject *args); 
static PyObject * resetAll_py(void);
static PyObject * setPeriod_py(PyObject *self, PyObject *args);
static PyObject * run_py(void);
static PyObject * stop_py(void);

#endif
