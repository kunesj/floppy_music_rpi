floppy_music_rpi
================

Playing music on floppy drives with raspberry pi

C code inspired by: https://github.com/SammyIAm/Moppy

Direction and step pins are defined in main.py and midikeyboard.py.

Install
-------
Install dependencies

    sudo apt-get install python-dev
    
wiringPi

    git clone git://git.drogon.net/wiringPi
    cd wiringPi
    ./build
  
floppy_sound_driver

    cd floppy_sound_driver_src
    sudo python setup.py install
    
Needed by midikeyboard.py

    sudo apt-get install python-pygame

Usage
-----
Run with

    sudo python main.py
    
Options

    -i --inputfile <file>
        Input midi file.
        
    -t --tracks <track1 track2 ...>
        Play only selected tracks (numbering starts with 0).
        
    -p --prints
        Show debug prints.

    --limitrange
        Limit range of played notes (C1 - B4).
        
Usage - midikeyboard
-----------------------
Run with

    sudo python midikeyboard.py
    
Options

    -l --listports
        Lists availible midi ports
        
    -p --port
        Select port to use

