#!/usr/bin/python2
import time
import midiparser
from operator import itemgetter
import sys
import argparse

import floppy_sound_driver as driver
from midifunctions import *

PIN_DIR = [8,7,16,1]#8,7 # direction pins for floppy drives
PIN_STEP = [9,15,0,2]#9,15 # step pins for floppy drives

# periods for each midi note no. 
# periods are in mikroseconds
basicPeriods = [122312, 115444, 108968, 102852, 97080, 91636, 86488, 81636, 77052, 72728, 68644, 64792] # C-1 - B-1

microPeriods_all = []
for j in range(0,9):
    for i in range(0,12):
        microPeriods_all.append(int(basicPeriods[i]/(2**j)))

# floppy drives are bad at playing outside range C1[30578] - B4[2025] => notes should be disabled
microPeriods_cut = [
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
30578, 28861, 27242, 25713, 24270, 22909, 21622, 20409, 19263, 18182, 17161, 16198, #C1 - B1 (24-)
15289, 14436, 13621, 12856, 12135, 11454, 10811, 10205, 9632, 9091, 8581, 8099, #C2 - B2
7645, 7218, 6811, 6428, 6068, 5727, 5406, 5103, 4816, 4546, 4291, 4050, #C3 - B3
3823, 3609, 3406, 3214, 3034, 2864, 2703, 2552, 2408, 2273, 2146, 2025, #C4 - B4 (-72)
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

def main():
    # input parser
    parser = argparse.ArgumentParser(
        description='Floppy midi player'
    )
    parser.add_argument(
        '-i', '--inputfile',
        default='example.mid',
        help='Input midi file')
    parser.add_argument(
        '-t', '--tracks',
        type=int, metavar='N', nargs='+',
        default=[None],
        help='Play only tracks N N N')
    parser.add_argument(
        '-p', '--prints',
        action='store_true',
        help='Show debug prints')
    parser.add_argument(
        '--limitrange',
        action='store_true',
        help='Limit range of played notes (C1 - B4)')

    args = parser.parse_args()
    
    if args.limitrange:
        microPeriods = list(microPeriods_cut)
    else:
        microPeriods = list(microPeriods_all)
    
    # parse midi
    midi = midiparser.File(args.inputfile)
    processed_midi = process_midi(midi)
    print processed_midi['info']
    
    # get notes
    notes = []
    if args.tracks == [None]:
        args.tracks = range(0, processed_midi['info']['Num. of Tracks'])
    for t in args.tracks:
        notes = notes + processed_midi['tracks'][t]['notes']
    notes = sorted(notes, key=itemgetter('start_time'))    
    notes.append({'start_time':-1}) # add end of midi

    # start floppy driver
    driver.init(PIN_DIR,PIN_STEP)
    driver.resetAll()
    driver.run()
    
    # current played note
    currentNote = []
    for i in range(len(PIN_DIR)):
            currentNote.append(None)
    currentNote_empty = list(currentNote)

    # Main loop
    start_time = time.time()
    while True:
        try:
            current_time = time.time() - start_time
            
            # detect end of notes
            if notes[0]['start_time'] == -1 and currentNote == currentNote_empty:
                print "end of notes"
                driver.stop()
                time.sleep(0.2)
                sys.exit()
        
            # end notes
            for i in range(len(PIN_DIR)):
                if currentNote[i] is not None:
                    if currentNote[i]['end_time']  <= current_time:
                        if args.prints:
                            print 'drive:', selected_drive, 'time_e:', round(currentNote[i]['end_time'], 3), 'note_off'
                        currentNote[i] = None
                        driver.setPeriod(i, 0)
        
            # play note
            while notes[0]['start_time'] <= current_time:
                # detect end
                if notes[0]['start_time'] == -1:
                    break

                selected_drive = -1
                min_time_i = -1
                for i in range(len(PIN_DIR)):
                    if currentNote[i] is None: # select idle drive
                        selected_drive = i
                        break
                    elif min_time_i == -1:
                        min_time_i = i
                    elif currentNote[min_time_i]['end_time'] > currentNote[i]['end_time']:
                        # Select drive with shortest note
                        min_time_i = i
                if selected_drive == -1:
                    selected_drive = min_time_i
            
                driver.setPeriod(i, microPeriods[notes[0]['note_no']])
                currentNote[i] = notes[0]
                notes = notes[1:]
                if args.prints:
                    print 'drive:', selected_drive, 'time_s:', round(currentNote[i]['start_time'], 3), 'note_no:', currentNote[i]['note_no'], 'time_e:', round(currentNote[i]['end_time'], 3)
            
            time.sleep(0.0025)
        
        except KeyboardInterrupt:
            print 'KeyboardInterrupt'
            driver.stop()
            time.sleep(0.2)
            sys.exit()

        except Exception, e:
            print str(e)
            driver.stop()
            time.sleep(0.2)
            sys.exit()
        
if __name__ == "__main__":
    main()
