#!/usr/bin/python2
from operator import itemgetter
import midiparser
    
def process_midi(midi):
    """
    processed_midi {}
        info {}
        tracks []
            notes []
            events [] # other than notes
    """
    # init + default values
    processed_midi = {'info':{}, 'tracks':[]}
    processed_midi['info']['SetTempo'] = {'amount':None, 'tempo':500000}
    
    # get midi info
    processed_midi['info']['File'] = midi.file
    processed_midi['info']['Format'] = midi.format
    processed_midi['info']['Num. of Tracks'] = midi.num_tracks
    processed_midi['info']['Division'] = midi.division 
    
    for track_raw in midi.tracks:
        for event in track_raw.events:
            if event.type == midiparser.meta.SetTempo:
                processed_midi['info']['SetTempo'] = {'amount':event.detail.amount, 'tempo':event.detail.tempo}
                
    # get tracks
    for track_raw in midi.tracks:
        track = {'notes':[], 'events':[]}
        notes_raw = []
        for event in track_raw.events:
            if event.type == midiparser.voice.NoteOn:
                notes_raw.append({'absolute':event.absolute, 'on':True, 'note_no':event.detail.note_no, 'velocity':event.detail.velocity})
            elif event.type == midiparser.voice.NoteOff:
                notes_raw.append({'absolute':event.absolute, 'on':False, 'note_no':event.detail.note_no})
                
                
            #elif event.type == midiparser.meta.TimeSignature:
                #midi_config['TimeSignature'] = {'log_denominator':event.detail.log_denominator, 'midi_clocks':event.detail.midi_clocks, 'numerator':event.detail.numerator, 'thirty_seconds':event.detail.thirty_seconds}
            #elif event.type == midiparser.meta.KeySignature:
                #midi_config['KeySignature'] = {'fifths':event.detail.fifths, 'mode':event.detail.mode}
            #elif event.type == midiparser.meta.EndTrack:
                #print 'EndTrack', dir(event)
            #elif event.type == midiparser.voice.ProgramChange:
                #midi_config['ProgramChange'] = {'amount':event.detail.amount}
            #elif event.type == midiparser.meta.TrackName:
                #print 'TrackName', dir(event)
            #elif event.type == midiparser.voice.ControllerChange:
                #print 'ControllerChange', dir(event)
            #elif event.type == midiparser.voice.PitchBend:
                #print 'PitchBend', dir(event)
            else:
                track['events'].append(event)
        
        # merge starts and ends of notes
        notes_raw = sorted(notes_raw, key=itemgetter('absolute'))
        tempo = processed_midi['info']['SetTempo']['tempo']
        division = processed_midi['info']['Division']
        for n in notes_raw:
            if n['on'] == False:
                continue # dont add ends of notes as notes
        
            for en in notes_raw:
                if (en['absolute'] > n['absolute']) and (en['note_no'] == n['note_no']):
                    # new press == end of old note
                    start_time = (n['absolute']/float(division))*(tempo/1000000.0)
                    end_time = (en['absolute']/float(division))*(tempo/1000000.0)
                    track['notes'].append({'start_time':start_time, 'end_time':end_time, 'note_no':n['note_no'], 'velocity':n['velocity']})
                    break
                    # not ending notes are ignored    
            
        processed_midi['tracks'].append(track)
        
    
    return processed_midi
    

def main():
    # parse midi
    midi = midiparser.File('example.mid')
    processed_midi = process_midi(midi)
    print processed_midi['info']
    
    # get notes
    notes = []
    for t in range(0, processed_midi['info']['Num. of Tracks']):
        notes = notes + processed_midi['tracks'][t]['notes']
    notes = sorted(notes, key=itemgetter('start_time'))    
    notes.append({'start_time':-1}) # add end of midi
    
    # print result
    for n in notes:
        print n

if __name__ == "__main__":
    main()
